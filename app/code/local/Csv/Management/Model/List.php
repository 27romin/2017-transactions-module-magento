<?php

class Csv_Management_Model_List extends Mage_Core_Model_Abstract
{
    public function getOrders()
    {
        $orders = Mage::getModel("sales/order")->getCollection()->addFieldToFilter('status', 'pending')->addAttributeToSelect('*');
        return $orders;
    }

    public function getByIncrementId($value)
    {
        $order = Mage::getModel("sales/order")->loadByIncrementId($value);
        return $order;
    }

    public function getItem($value){
        $order = Mage::getModel('sales/order')->load($value);
        return $order;
    }

    public  function getCategory($value){
        $category = Mage::getModel('catalog/category')->load($value);
        return $category;
    }
}

