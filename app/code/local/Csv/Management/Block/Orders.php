<?php
class Csv_Management_Block_Orders extends Mage_Core_Block_Template
{
    public function getOrders(){
        $arr_products = array();
        $orders = Mage::getModel('csv_management/list')->getOrders();

        foreach($orders as $order){
            $arr_products[] = array(
                'increment_id' => $order->getIncrementId(),
                'created_at' => $order->getCreatedAt(),
            );
        }
        return $arr_products;
    }
}