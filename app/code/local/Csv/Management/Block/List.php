<?php

class Csv_Management_Block_List extends Mage_Core_Block_Template
{
    public function getMagentoOrders()
    {
        $arr_products = array();
        $orders = Mage::getModel('csv_management/list')->getOrders();
        foreach ($orders as $order) {
            $arr_products[] = array(
                'increment_id' => $order->getIncrementId(),
                'created_at' => $order->getCreatedAt()
            );
        }
        return $arr_products;
    }

    public function generateURL($counter, $url, $sku, $name, $category, $price, $quantity)
    {
        $url .= 'sku' . $counter . '=' . $sku . '&'
            . 'name' . $counter . '=' . str_replace(' ', '_', $name) . '&'
            . 'category' . $counter . '=' . str_replace('&', 'And', str_replace(' ', '_', $category)) . '&'
            . 'price' . $counter . '=' . str_replace('£', '', $price) . '&'
            . 'quantity' . $counter . '=' . $quantity . '&';
        return $url;
    }

    public function getOrderItem($value)
    {
        $arr_products = array();
        $orders = Mage::getModel('csv_management/list')->getItem($value)->getAllItems();
        foreach ($orders as $order) {
            $arr_products[] = array(
                'sku' => $order->getSku(),
                'name' => $order->getName(),
                'quantity' => (int)$order->getQtyOrdered(),
                'price' => $this->formatPrice($order->getPrice()),
                'category_id' => $order->getProductId(),
            );
        }
        return $arr_products;
    }

    public function formatPrice($value)
    {
        $price = Mage::helper('core')->currency($value, true, false);
        return $price;
    }

    public function setCsvOrders($orders)
    {
        if (is_array($orders) && count($orders)) {
            foreach ($orders as $record) {
                $compare[] = str_replace('M', '', $record['line']);
            }
        }
        return $compare;
    }

    public function setMagentoOrders($orders)
    {
        if (is_array($orders) && count($orders)) {
            foreach ($orders as $record) {
                $compare[] = $record['increment_id'];
            }
        }
        return $compare;
    }

    public function compareOrders($magento, $csv)
    {
        $difference = array_diff($magento, $csv);
        $arr_products = array();
        foreach ($difference as $order) {
            $product = Mage::getModel('csv_management/list')->getByIncrementId($order);
            $arr_products[] = array(
                'order_id' => $product->getEntityId(),
                'increment_id' => $product->getIncrementId(),
                'created_at' => $product->getCreatedAt(),
            );
        }
        return $arr_products;
    }

    public function getCategoryName($value)
    {
        $category = Mage::getModel('csv_management/list')->getCategory($value);
        return $category->getName();
    }

    public function generateTextFile($file)
    {
        $file_file = "links/links.txt";
        $fh = fopen($file_file, 'w') or die("cannot open file");
        if (is_array($file)) {
            foreach ($file as $record) {
                $order = Mage::getModel('csv_management/list')->getByIncrementId($record['increment_id']);
                $string = 'http://WEBSITE/URI/?append=' . $order->getIncrementId() . "\n";
                fwrite($fh, $string);
            }
        }
        fclose;
    }
}