<?php

class Csv_Management_Block_Push extends Mage_Core_Block_Template
{
    public function getFullUrl()
    {
        return $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }

    public function setURL($params)
    {
        $counter = 0;
        while ($counter < $params) {
            $counter++;
            $arr_vars[] = array(
                'product' => $this->getRequest()->getParam('product'),
                'sku' . $counter => $this->getRequest()->getParam('sku' . $counter),
                'name' . $counter => $this->getRequest()->getParam('name' . $counter),
                'category' . $counter => $this->getRequest()->getParam('category' . $counter),
                'price' . $counter => $this->getRequest()->getParam('price' . $counter),
                'quantity' . $counter => $this->getRequest()->getParam('quantity' . $counter),
            );
        }
        return $arr_vars;
    }
}