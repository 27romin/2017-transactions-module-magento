<?php

class Csv_Management_Block_Csv extends Mage_Core_Block_Template
{
    protected $file;
    protected $directory;
    protected $directory_file;
    protected $latest_file;

    public function __construct()
    {
        $this->directory = './uploads';
        $this->directory_file = scandir($this->directory, 1);
        $this->latest_file = $this->directory_file[0];
        $this->file = fopen($this->directory . "/" . $this->latest_file, "r");
    }

    public function getCsv()
    {
        $count = 0;
        foreach ($this->directory_file as $file) {
            if ($file != '.' && $file != '..') {
                $arr_csv[] = array('file' => $file,);
            }
            $count++;
        }
        return $arr_csv;
    }

    public function getCsvRecords()
    {
        while (!feof($this->file)) {
            $line = fgetcsv($this->file, 1024);
            if (is_numeric($line[0]) or strpos($line[0], 'M') !== false) {
                $arr_csv_records[] = array(
                    'line' => $line[0]
                );
            }
        }
        fclose($this->file);
        return $arr_csv_records;
    }
}